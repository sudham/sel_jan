package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class getDataFromXcel  {
	
	static Object[][] data;
	
	@Test
	public static Object[][] getDataExcel(String dataSheet) {	

		// Enter the file name

		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./Data/"+dataSheet+".xlsx");
		// get the worksheet
		XSSFSheet wsheet = wbook.getSheet("Sheet1");
		// Get the row count
		int rowCount = wsheet.getLastRowNum();
		System.out.println("row count is-->" + rowCount);
		// Get the column count
		int columnCount = wsheet.getRow(0).getLastCellNum();
		System.out.println(columnCount);
		data=new Object[rowCount][columnCount];
		// get the row for all the rows in a loop
		for (int i = 1; i <= rowCount; i++) {
			XSSFRow everyrow = wsheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				XSSFCell cell = everyrow.getCell(j);
				data[i-1][j]=cell.getStringCellValue();
			}

		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	return data;
	
	
	}
}
