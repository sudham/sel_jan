package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.Reports;

public class SeleniumBase extends Reports implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	public Alert alertFucntions;
	WebDriverWait wait;

	
	@Override
	public void startApp(String url) {
		try {
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get(url);
			logStep("The Url "+url+" Opened successfully","pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logStep("The Url "+url+" does not Opened successfully, MalformedURL","fail");

		}
	    takeSnap();
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
			logStep("The browser "+browser+" launched successfully","pass");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logStep("The Browser Could not be Launched. Hence Failed","fail");
			throw new RuntimeException();

		}finally {
        takeSnap();
		}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "LinkText":return driver.findElementByLinkText(value);
			case "PartialLinkText":return driver.findElementByPartialLinkText(value);
			case "TagName":return driver.findElementByTagName(value);
				}
			logStep("The field has been located"+locatorType+"successfully","pass");
		} catch (NoSuchElementException  e) {
			logStep("The Element with locator:"+locatorType+" Not Found with value: "+value,"fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		return driver.findElementById(value);	
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type.toLowerCase()) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "xpath": return driver.findElementsByXPath(value);
			case "LinkText":return driver.findElementsByLinkText(value);
			case "PartialLinkText":return driver.findElementsByPartialLinkText(value);
			case "TagName":return driver.findElementsByTagName(value);
}
			logStep("The field has been located"+type+"successfully","pass");
		} catch (NoSuchElementException  e) {
			logStep("The Element with locator:"+type+" Not Found with value: "+value,"fail");
			throw new RuntimeException();

		}
		finally {
		takeSnap();
		}
		return null;
	}
	@Override
	public void switchToAlert() {
		try {
			alertFucntions = driver.switchTo().alert();
			logStep("Switched to the alert successfully","pass");
			
		} catch (Exception e) {
			logStep("NoAlertPresentException","fail");
			
		}

	}

	@Override
	public void acceptAlert() {
		try {
			alertFucntions.accept();
			logStep("The alert "+alertFucntions.getText()+" is accepted.","pass");

		} catch (NoAlertPresentException  e) {
			logStep("NoAlertPresentException","fail");
		} catch (WebDriverException  e) {
			logStep("WebDriverException : "+e.getMessage(),"fail");
		}  
	}

	@Override
	public void dismissAlert() {
		try {
			alertFucntions.dismiss();
			logStep("The alert "+alertFucntions.getText()+" is accepted.","pass");

		} catch (NoAlertPresentException  e) {
			logStep("There is no alert present.","fail");
		} catch (WebDriverException  e) {
			logStep("WebDriverException : "+e.getMessage(),"fail");
		}  
	}

	@Override
	public String getAlertText() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			logStep("The alert "+alertFucntions.getText()+" is displayed correctly.","pass");
		} catch (NoAlertPresentException e) {
			logStep("There is no alert present.","fail");
		} catch (WebDriverException e) {
			logStep("WebDriverException : "+e.getMessage(),"fail");
		} 
		return text;

	}

	
	@Override
	public void typeAlert(String data) {
		alertFucntions.sendKeys(data);
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowPages = driver.getWindowHandles();
			List<String> lsWindowsPages = new ArrayList<String>();
			lsWindowsPages.addAll(windowPages);
			driver.switchTo().window(lsWindowsPages.get(index));
			logStep("Switched to another window successfully","pass");
		} catch (NoSuchWindowException  e) {
			logStep("The Window With index: "+index+
					" not found","fail");	
		}
		finally {
			takeSnap();
		}
		
	}

	@Override
	public void switchToWindow(String title) {
		try {
				Set<String> allWindows = driver.getWindowHandles();
				for (String eachWindow : allWindows) {
					driver.switchTo().window(eachWindow);
					if (driver.getTitle().equals(title)) {
						break;
					}
				}
				logStep("The Window With Title: "+title+
						"is switched ","pass");
			} catch (NoSuchWindowException e) {
				logStep("The Window With Title: "+title+
						" not found","fail");
			} finally {
				takeSnap();
			}
		
	}

	@Override
	public void switchToFrame(int index) {
		
	    try {
			driver.switchTo().frame(index)	;
			logStep("Swicted to frame successfully","pass");
		} catch (NoSuchFrameException e) {
			logStep("NoSuchFrameException","fail");
		}
	    catch (StaleElementReferenceException e) {
		       logStep("The webelement is not present during the run time","fail");
		}
	    finally {
	    	takeSnap();
	    }

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			logStep("Switched to frame sucessfully","pass");
		} catch (NoSuchFrameException e) {
		 logStep("NoSuchFrameException","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			logStep("Switched to frame sucessfully","pass");
		} catch (NoSuchFrameException e) {
			// TODO Auto-generated catch block
			 logStep("NoSuchFrameException","fail");

		}finally {
			takeSnap();
		}

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			logStep("Switched to default content","pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logStep("NoSuchcontentexception","fail");
		}finally {
		takeSnap();
		}
	}

	@Override
	public boolean verifyUrl(String url) {
		if (url.equals(driver.getCurrentUrl())) {
			logStep("Url is verified","pass");
			return true;
		} else {
			logStep("Url is not verified","fail");
			return false;
		}
	}

	@Override
	public boolean verifyTitle(String title) {
		System.out.println(driver.getTitle());
		if (title.equals(driver.getTitle())) {
			logStep("Title is correct","pass");
			return true;
		} else {
			logStep("Title is not correct","fail");
			return false;
		}
	}
		   
	

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		driver.close();
	}

	@Override
	public void quit() {
		driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			logStep("The Element "+ele+" clicked", "pass"); 
		} catch (StaleElementReferenceException e) {
			logStep("The Element "+ele+" could not be clicked", "fail");
			throw new RuntimeException();
		} finally { 
			takeSnap();
		}
		

	}
	public void clickWithNoSnap(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			logStep("The Element "+ele+" clicked","pass");
		} catch (StaleElementReferenceException e) {
			logStep("The Element "+ele+" could not be clicked","fail");
			throw new RuntimeException();
		}

	}



	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data); 
			logStep("The data "+data+" append successfully","pass");
			takeSnap();
		} catch (ElementNotInteractableException e) {
			logStep("Element is not Interactable","fail");
		
		}
	     catch (IllegalArgumentException e) {
			logStep("Data is null,IllegalArgumentException","fail");
		}
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			logStep("The field is cleared Successfully","pass");

		} catch (InvalidElementStateException e) {
			logStep("The field is not Interactable","fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data); 
			logStep("The data "+data+" entered successfully","pass");
			
		} catch (ElementNotInteractableException e) {
			// TODO Auto-generated catch block
		logStep("Data not present","fail");
		}
		catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
		logStep("Data is null","fail");
		}finally {
			takeSnap();
		}
		
	}

	@Override
	public String getElementText(WebElement ele) {
         return  ele.getText();
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
	return getBackgroundColor(ele);
	}

	@Override
	public String getTypedText(WebElement ele) {
    return getTypedText(ele);
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select selDropDown = new Select(ele);
			selDropDown.selectByVisibleText(value);
			logStep("Data is selected successfully","pass");
	
		} catch (NoSuchElementException e) {
			logStep("No Such element present in the page","fail");
		}finally {
			takeSnap();
		}
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select selDropDown = new Select(ele);
			selDropDown.selectByIndex(index);
			logStep("Data is selected successfully by using Index","pass");
			
		} catch (NoSuchElementException e) {
			logStep("No Such element present in the page","fail");
		}finally {
			takeSnap();
		}
	
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select selDropDown = new Select(ele);
			selDropDown.selectByValue(value);
			logStep("Data is selected successfully by using value","pass");
		} catch (NoSuchElementException e) {
			logStep("No Such element present in the page","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().equals(expectedText)) {
				logStep("The expected text contains the actual "+expectedText,"pass");
				return true;
			}else {
				logStep("The expected text doesn't contain the actual "+expectedText,"pass");
			}
		} catch (WebDriverException e) {
			logStep("Unknown exception occured while verifying the Text","fail");
		} finally {
			takeSnap();
		}

		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().contains(expectedText)) {
				logStep("The expected text contains the actual "+expectedText,"pass");
				return true;
			}else {
				logStep("The expected text doesn't contain the actual "+expectedText,"pass");
			}
		} catch (WebDriverException e) {
			logStep("Unknown exception occured while verifying the Text","fail");
		} finally {
			takeSnap();
		}

		return false;

	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(value).equals(value)) {
				logStep("Expected Text is available in the webelement ","pass");
				return true;	
			}else 
				logStep("Expected Text is not available in the webelement","pass");
		} catch (WebDriverException e) {
			logStep("Unknown exception occured while verifying the Attribute Text","fail");
		}finally {
			takeSnap();
		}
					return false;	
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).contains(value)) {
				logStep("The expected attribute :"+attribute+" value contains the actual "+value,"pass");
			}else {
				logStep("The expected attribute :"+attribute+" value does not contains the actual "+value,"pass");
			}
		} catch (WebDriverException e) {
			logStep("Unknown exception occured while verifying the Attribute Text","fail");
		}finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
         try {
			if(ele.isDisplayed()) {
				 logStep("Element is displayed","pass");
					return false;
			 }else {
				 logStep("Element is not displayed","pass");
     
			 }
		} catch (WebDriverException  e) {
			// TODO Auto-generated catch block
			logStep("WebDriverException : "+e.getMessage(),"fail");
		}finally{
			takeSnap();
		}
 		return false;

	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if(!ele.isDisplayed()) {
       	 logStep("Element is invisbile ","pass");
       		return true;
        }else {
       	 logStep("Element is visible","pass");
       		return false;
        }
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			if(ele.isEnabled()) {
			   	 logStep("Element is enabled","pass");
			   		return true;
			    }else {
			   	 logStep("Element is not enabled","pass");
			   		
			    }
		} catch (WebDriverException  e) {
			// TODO Auto-generated catch block
			logStep("WebDriverException : "+e.getMessage(),"fail");
		}finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
			   	 logStep("Element is selected","pass");
			   		return true;
			    }else {
			   	 logStep("Element is not selected","pass");
			    }
		} catch (WebDriverException  e) {
			// TODO Auto-generated catch block
			logStep("WebDriverException : "+e.getMessage(),"fail");

		}finally {
			takeSnap();
		}
		return false;
	}
	
	public WebElement locateElementTable(WebElement ele, int rowindex,int columnindex){
	    try {
			List<WebElement> rowList = ele.findElements(By.tagName("tr"));
			   int rowCount = rowList.size();
			   System.out.println(rowCount);
			   WebElement intRow = rowList.get(rowindex);
			  List<WebElement> columnList = intRow.findElements(By.tagName("td"));
			  return columnList.get(columnindex);
					 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null; 
	 
	}
}


