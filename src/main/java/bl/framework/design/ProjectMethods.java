package bl.framework.design;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import bl.framework.api.SeleniumBase;
import utils.getDataFromXcel;

public class ProjectMethods extends SeleniumBase {
	
	public String testcaseName, testcaseDescription, author, category,dataSheet ;
	
	@BeforeSuite
	public void beforeSuite() {
		startReport();
	}
	@AfterSuite
	public void afterSuite() {
		endReport();
	}
	@BeforeClass
	public void beforeClass() throws IOException {
		initializeTest(testcaseName, testcaseDescription, author, category);
	}

	
	@BeforeMethod
	public void Login() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		WebElement eleCRMLink = locateElement("xpath","//a[contains(text(),'CRM/SFA')]");
		click(eleCRMLink);
	}
	
		//Mehtod to invoke test data excel
		
	@DataProvider(name="fetchData")
		public Object[][] getData( ) {	
			return getDataFromXcel.getDataExcel(dataSheet);
			
		}
	
}

