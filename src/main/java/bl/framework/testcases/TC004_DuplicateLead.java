package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC004_DuplicateLead extends SeleniumBase {
		
		@Test
		public void duplicateLead() {
			WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
			click(eleLeads);
			WebElement eleFindLeads = locateElement("xpath", "//a[contains(text(),'Find Leads')]");
			click(eleFindLeads);
			WebElement eleEmail = locateElement("xpath", "//span[text()='Email']");
			click(eleEmail);
			WebElement eleEmailAddress = locateElement("xpath", "//input[@name='emailAddress']");
			clearAndType(eleEmailAddress, "sss@sss.com");
			WebElement eleFirstRecord = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
			click(eleFirstRecord);
			WebElement eleDuplicateButton = locateElement("xpath","//a[text()='Duplicate Lead']");
			click(eleDuplicateButton);
			verifyTitle("Duplicate Lead");
			WebElement eleCreateLead = locateElement("xpath","//input[@name='submitButton']");
			click(eleCreateLead);
			close();
			


}
}
