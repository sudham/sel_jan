package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;


public class TC003_EditLead extends ProjectMethods{
	
@BeforeTest
	
	public void setdata() {
	
	       testcaseName="TC002_EditLead";
			testcaseDescription="Edit of lead";
			author="Sudha";
			category="smoke";
			dataSheet="Testdata";
	}

	
	//@Test(dependsOnMethods= {"bl.framework.testcases.TC002_CreateLead.java")}
	
	@Test(dataProvider="fetchData")
	public void editLead(String cname,String fname,String lname) {
		
		WebElement eleLeads = locateElement("xpath", "//a[text()='Leads']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[contains(text(),'Find Leads')]");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "//label[text()='First name:']//following::input[30]");
		clearAndType(eleFirstName, fname);
		WebElement eleLastName = locateElement("xpath", "//label[text()='Last name:']//following::input[29]");
		clearAndType(eleLastName, lname);
		WebElement eleFindLeadsnew = locateElement("xpath", "//button[contains(text(),'Find Leads')]");
		click(eleFindLeadsnew);
		WebElement eleFindFirstRow=locateElement("xpath", "//table[@class='x-grid3-row-table']");
	//	WebElement eleFindElementSelect=locateElementTable(eleFindFirstRow,1,1);
	//	click(eleFindElementSelect);
		WebElement eleFindFirstRow1 = locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a");
		click(eleFindFirstRow1);
		verifyTitle("View Lead");
		WebElement eleEdit = locateElement("xpath", "//a[text()='Edit']");
		click(eleEdit);
		WebElement elementCompanyName = locateElement("id", "updateLeadForm_companyName");
		clearAndType(elementCompanyName, "Test");
		WebElement elementUpdate = locateElement("xpath", "//input[@name='submitButton']");
		click(elementUpdate);
		WebElement viewUpdateName = locateElement("id", "viewLead_companyName_sp");
		verifyPartialText(viewUpdateName, "Test");
		close();
	
}
}