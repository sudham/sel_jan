package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC001_LoginAndLogout extends SeleniumBase{

	@Parameters({"url","username","password"})
	@Test
	public void login(String url,String uname,String pass ) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pass); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
//		WebElement eleLogout = locateElement("class", "decorativeSubmit");
//		click(eleLogout);
	}
}








