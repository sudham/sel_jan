package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	//Create Lead 
	
@BeforeTest
	
	public void setdata() {
	
	       testcaseName="TC002_CreateLead";
			testcaseDescription="Creation of lead";
			author="Sudha";
			category="smoke";
			dataSheet="Testdata";
	}
	
	//@Test(invocationCount=2,invocationTimeOut=50000)
	
	@Test(dataProvider="fetchData")
	public void createLead(String cname, String fname,String lname)  {
		
		WebElement eleCreateLead = locateElement("xpath","//a[text()='Create Lead']");
		click(eleCreateLead);
		WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
		clearAndType(eleCompanyName,cname);
		WebElement elefirstName = locateElement("id","createLeadForm_firstName");
		clearAndType(elefirstName,fname);
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		clearAndType(eleLastName,lname);
		WebElement eleSubmitCreateLead = locateElement("name","submitButton");
		click(eleSubmitCreateLead);
		verifyTitle("View Lead | opentaps CRM");
		WebElement elsViewCompanyName = locateElement("id","viewLead_companyName_sp");
		 getElementText(elsViewCompanyName);
		verifyExactText(elsViewCompanyName,"Test leaf");
	    close();
		
		}
	
	@DataProvider(name="getData")
	public String[][] testData() {
		
		String[][] getData=new String[2][3];
		getData[0][0]="Test Leaf";
		getData[0][1]="Sudha";
		getData[0][2]="M";
		
		getData[1][0]="Test Leaf";
		getData[1][1]="Veera";
		getData[1][2]="R";
		
		return getData;
		
	}
	
	@DataProvider(name="getData1")
public String[][] testData1() {
		
		String[][] getData=new String[2][3];
		getData[0][0]="Test Leaf";
		getData[0][1]="Kavin";
		getData[0][2]="v";
		
		getData[1][0]="Test Leaf";
		getData[1][1]="Yuvan";
		getData[1][2]="V";
		
		return getData;
		
	}

		
		
		
		
	}


