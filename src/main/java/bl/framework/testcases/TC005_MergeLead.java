package bl.framework.testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {
	
@BeforeTest
	
	public void setdata() {
	
	       testcaseName="TC002_MergeLead";
			testcaseDescription="Merge of lead";
			author="Sudha";
			category="smoke";
			dataSheet="Testdata";
	}

	@Test
	public void MergeLead() throws InterruptedException {

		WebElement eleTextLead = locateElement("xpath", "//a[text()='Leads']");
		click(eleTextLead);
		WebElement eleMergeLead = locateElement("xpath", "//a[text()='Merge Leads']");
		click(eleMergeLead);
		WebElement eleImage = locateElement("xpath", "//input[@id='partyIdFrom']/following::img");
		click(eleImage);
		switchToWindow(1);
		WebElement eleLeadId = locateElement("name", "id");
		clearAndType(eleLeadId, "10039");
		WebElement eleFindLead = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLead);
		Thread.sleep(30000);
		WebElement eleFirstRecord = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstRecord);
		//switchToWindow(0);
		WebElement eleImage1 = locateElement("xpath", "//input[@id='partyIdTo']/following::img");
		click(eleImage1);
//		WebElement eleToLead = locateElement("LinkText", "Merge");
//		click(eleToLead);
		switchToWindow(1);
		WebElement eleLeadId1 = locateElement("name", "id");
		clearAndType(eleLeadId1, "10007");
		WebElement eleFindLead1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLead1);
		Thread.sleep(30000);
		WebElement eleFirstRecord1 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleFirstRecord1);
		defaultContent();
		WebElement eleMerge = locateElement("xpath", "//a[text()='Merge']");
		click(eleMerge);
		switchToAlert();
		alertFucntions.accept();
		WebElement eleFindLeads1 = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads1);
		WebElement eleid = locateElement("name", "id");
		clearAndType(eleid, "10007");
		WebElement eleFindLeads1submit = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads1submit);
		Thread.sleep(10000);

		close();
		quit();

	}

}
