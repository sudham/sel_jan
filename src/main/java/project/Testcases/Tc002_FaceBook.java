package project.Testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class Tc002_FaceBook {

	@Test
	public void faceBook() throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		// to disable notifications.

		ChromeOptions op = new ChromeOptions();
		op.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(op);

		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

//Login(

		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("trustme.sudha");
		driver.findElementById("pass").sendKeys("blacky83");
		driver.findElementByXPath("//input[@type='submit']").click();

		// Search and choose Test Leaf

		Thread.sleep(1000);
		driver.findElementByXPath("//input[2][@placeholder='Search']").clear();
		driver.findElementByXPath("//input[2][@placeholder='Search']").sendKeys("Testleaf");
		driver.findElementByXPath("//div[@role='search']/form/button/i").click();

		// selecting from the data displayed below

//	Thread.sleep(2000);
//	
//	String searchTitle = driver.findElementByXPath("//a[contains(@href,'https://www.facebook.com/search/top/?q=testleaf')]").getText();
//	System.out.println(searchTitle);
//	if(searchTitle.equalsIgnoreCase("testleaf")) {
//		driver.findElementByXPath("//a[contains(@href,'https://www.facebook.com/search/top/?q=testleaf')]").click();
//	}
//	
		// select the test leaf in places

		driver.findElementByXPath("//div[2][contains(text(),'Places')]/../..").click();

		// verify whether test leaf is displayed

		Thread.sleep(3000);
		String searchTitle1 = driver
				.findElementByXPath("//a[contains(@href,'https://www.facebook.com/TestleafSolutionsIncChennai/')]")
				.getText();
		System.out.println("search title is...>" + searchTitle1);
		if (searchTitle1.equalsIgnoreCase("testleaf")) {
			driver.findElementByXPath("//a[contains(@href,'https://www.facebook.com/TestleafSolutionsIncChennai/')]")
					.click();

		}
//printing number of likes in the page 
		
		String noOfLikes = driver.findElementByXPath("//div[contains(text(),' people like this')]").getText();
		System.out.println("Number of likes--->" + noOfLikes);

		Thread.sleep(1000);

		String titleLike = driver.findElementByXPath("//span[2][contains(text(),'Liked')]").getText();
		if (titleLike.equalsIgnoreCase("liked")) {
			System.out.println("Already liked");
		} else {
			String likeTitle = driver.findElementByXPath("//button[contains(@class,'likeButton')]").getText();
				if (likeTitle.equalsIgnoreCase("like")) {
					System.out.println("click on link");
					driver.findElementByXPath("//button[contains(@class,'likeButton')]").click();
				}
			}

			String titleHead = driver.findElementByXPath("//span[contains(text(),'TestLeaf')]").getText();
			if (titleHead.equalsIgnoreCase("testleaf")) {
				System.out.println("Title is ==>" + titleHead);
			}
			driver.close();
		}

	}


