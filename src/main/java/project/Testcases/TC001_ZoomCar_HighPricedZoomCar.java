package project.Testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC001_ZoomCar_HighPricedZoomCar {

	@Test
	public void HighPricesZoomCar() throws InterruptedException {
		// start url
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.findElementByXPath("//a[@title='Start your wonderful journey']").click();
		driver.findElementByXPath("//div[text()='Popular Pick-up points']//following::div[1]").click();
		driver.findElementByClassName("proceed").click();

		// Get current date

		driver.findElementByXPath("//div[@class='day picked ']/following::div[1]").click();
		driver.findElementByClassName("proceed").click();

		Thread.sleep(2000);

		// confirm the start as tommorrow

		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		Integer tommorrow = Integer.parseInt(today) + 1;
		System.out.println(tommorrow);

		String currentDate = driver.findElementByXPath("//div[@class='day picked low-price']").getText();
		System.out.println("Current date is " + currentDate);

		if (currentDate.contains(tommorrow.toString())) {
			System.out.println("Start date is tommoroww");
			driver.findElementByClassName("proceed").click();
		}

		// Check the highest price

		List<WebElement> lsPrice = driver.findElementsByXPath("//div[@class='price']");
		List<Integer> lsNum = new ArrayList();
		for (WebElement eachLsPrice : lsPrice) {
			String price = eachLsPrice.getText().replaceAll("\\D", "");
			System.out.println(price);
			lsNum.add(Integer.parseInt(price));

		}
		// Count of the list

		int size = lsNum.size();

		// Sort the Integer and get the brand name

		Collections.sort(lsNum);
		Integer position = lsNum.get(size - 1);
		System.out.println("Count is" + position);

		// get brand Name

		driver.findElementByXPath("//div[contains(text(),'" + position + "')]//ancestor::div[@class='car-item']//h3");
				
		String brandName = driver
				.findElementByXPath("//div[contains(text(),'" + position + "')]//ancestor::div[@class='car-item']//h3")
				.getText();
		System.out.println("Brand name is" + brandName);
		
		//book  now
		
		driver.findElementByXPath("//div[contains(text(),'" + position + "')]//following::button[1]");

		// closing the browser

		driver.close();
	}

}
